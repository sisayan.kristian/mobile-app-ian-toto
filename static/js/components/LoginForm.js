import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
} from 'react-native';

export default class LoginForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username: '',
      password: '',
      showPassword: false,
      blockLogin: true,
    };
  
    this.handleTextChange = this.handleTextChange.bind(this);
    this.checkLoginForm = this.checkLoginForm.bind(this);
  }

  checkLoginForm() {
    const { username } = this.state;
    console.log(username);
    if(this.state.username !== '' && this.state.password !== ''){
      this.setState({
        blockLogin: false,
      });
    } else this.setState({
      blockLogin: true,
    });
  }

  handleTextChange(name, text){
    this.setState({
      [name]: text,
    }, () => this.checkLoginForm());
  }

  render() {
    const {
      username,
      password,
      showPassword,
      blockLogin,
    } = this.state;

    return (
      <View style={styles.container}>
        <TextInput
          name='username'
          value={username}
          placeholder='Username'
          style={styles.textField}
          onChangeText={(text) => this.handleTextChange('username', text)}
        />
        <TextInput
          name='password'
          value={password}
          placeholder='Password'
          secureTextEntry={true}
          style={styles.textField}
          onChangeText={(text) => this.handleTextChange('password', text)}
        />
        <Button
          name='login'
          disabled={blockLogin}
          title='Login'
          style={styles.login}
          onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#99FFFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  login: {
    backgroundColor: 'gray',
    borderColor: '#f1f1f1',
    margin: 40,
  },
  textField: {
    borderColor: '#f1f1f1',
    borderWidth: 1,
    height: 30,
    width: 200,
    margin: 20,
    padding: 5,
  }
});
