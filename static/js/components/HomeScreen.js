import React from 'react';
import {
    Text,
    View,
    TextInput,
    StyleSheet,
    Button,
} from 'react-native';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Button
                  name='equipmentRequest'
                  title='Equipment Request'
                  onPress={() => this.props.navigation.navigate('EquipmentRequest')}
                />
                <Button
                  name='healthConcerns'
                  title='Health Concerns'
                  onPress={() => this.props.navigation.navigate('HealthConcerns')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#99FFFF',
      alignItems: 'center',
      justifyContent: 'center',
    },
    login: {
      backgroundColor: '#000000',
      borderColor: '#000000',
      margin: 40,
    },
  });