import React from 'react';
import {
    Text,
    View,
    TextInput,
    StyleSheet,
    Button,
    Picker,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

export default class EquipmentRequestForm extends React.Component {
    constructor(props) {
        super(props);
        this.state=({
            reason: '',
            selectedEquipment: '',
            items: [
                {
                    label: 'Equipment 1',
                    value: 'equipment1',
                },
                {
                    label: 'Equipment 2',
                    value: 'equipment2',
                },
                {
                    label: 'Equipment 3',
                    value: 'equipment3',
                },
            ],
        });

    }

    onChangeHandler(name, text){
        this.setState({
            [name]: text,
        });
    }

    render() {
        const { reason, selectedEquipment, items } = this.state;
        return (
            <View>
                <View name='equipmentField' style={styles.formField}>
                    <Text style={styles.titleText}>Equipment:</Text>
                    <RNPickerSelect
                        placeholder={{ label: 'Select an equipment', value: null}}
                        onValueChange={(itemValue) => this.onChangeHandler('selectedEquipment', itemValue)}
                        value={selectedEquipment}
                        items={items}
                        style={styles.dropdown}
                    >
                    </RNPickerSelect>
                </View>
                <View name='reasonField' style={styles.formField}>
                    <Text style={styles.titleText}>Reason:</Text>
                    <TextInput 
                        name='reason'
                        value={reason}
                        onChangeText={(text) => this.onChangeHandler('reason', text)}
                        placeholder='Input a reason'
                        style={styles.textField}
                    />
                </View>
                <Button
                  name='submit'
                  title='Submit'
                  onPress={() => this.props.navigation.navigate('Home')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#99FFFF',
      alignItems: 'center',
      justifyContent: 'center',
    },
    formField: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginTop: 10,
      marginBottom: 10,
    },
    titleText: {
      marginRight: 5,
    },
    dropdown: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
    formTitle: {
        alignItems: 'center',
    },
    textField: {
        borderColor: '#f1f1f1',
        borderWidth: 1,
        height: 20,
        width: 200,
        padding: 2,
      },
  });