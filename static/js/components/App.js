import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import LoginForm from './LoginForm';
import Home from './HomeScreen';
import EquipmentRequestForm from './EquipmentRequest';
import HealthConcernsForm from './HealthConcerns';

class App extends React.Component {
    render() {
        return (
            <ApplicationStackNavigator />
        );
    }
}


const ApplicationStackNavigator = createStackNavigator ({
    Login: LoginForm,
    Home: Home,
    EquipmentRequest: EquipmentRequestForm,
    HealthConcerns: HealthConcernsForm,
}, {
    initialRouteName: "Login"
});

export default createAppContainer(ApplicationStackNavigator);
