import React from 'react';
import {
    Text,
    View,
    TextInput,
    StyleSheet,
    Button,
} from 'react-native';

export default class HealthConcernsForm extends React.Component {
    constructor(props) {
        super(props);
        this.state=({
            serialNum: '',
            concern: '',
        });

        this.onChangeHandler = this.onChangeHandler.bind(this);
    }

    onChangeHandler(name, text){
        this.setState({
            [name]: text,
        });
    }

    render() {
        const { serialNum, concern } = this.props;
        return (
            <View>
                <View name='dogSerialNumber' style={styles.formField}>
                    <Text style={styles.titleText}>Dog Serial Number:</Text>
                    <TextInput 
                        name='serialNum'
                        value={serialNum}
                        onChangeText={(text) => this.onChangeHandler('serialNum', text)}
                        placeholder='Input dog serial number'
                        style={styles.textField}
                        />
                </View>
                <View name='concern' style={styles.concernForm}>
                <Text style={styles.titleText}>Concern:</Text>
                    <TextInput 
                        name='concern'
                        value={concern}
                        onChangeText={(text) => this.onChangeHandler('concern', text)}
                        placeholder='Input concern'
                        style={styles.textConcern}
                        />
                </View>
                <Button
                  name='submit'
                  title='Submit'
                  onPress={() => this.props.navigation.navigate('Home')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#99FFFF',
      alignItems: 'center',
      justifyContent: 'center',
    },
    concernForm: {
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    formField: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginTop: 10,
      marginBottom: 10,
    },
    titleText: {
      marginRight: 5,
    },
    textField: {
        borderColor: '#f1f1f1',
        borderWidth: 1,
        height: 20,
        width: 200,
        padding: 2,
      },
    textConcern: {
        borderColor: '#f1f1f1',
        borderWidth: 1,
        height: 50,
        width: 300,
        padding: 2,
    }
  });